<?php declare(strict_types=1);

// namespace DoctrineMigrations; For dev
namespace App\Migrations; // For test/prod

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ticket #22460
 */
final class Version20210121085913 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DELETE FROM `paragraph` WHERE `id` = 24');
        $this->addSql('DELETE FROM `paragraph` WHERE `id` = 25');
        $this->addSql('DELETE FROM `paragraph` WHERE `id` = 26');
        $this->addSql('DELETE FROM `section` WHERE `id` = 24');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
