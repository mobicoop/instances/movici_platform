<?php

declare(strict_types=1);

// namespace DoctrineMigrations; For dev

namespace App\Migrations; // For test/prod

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ticket #23129.
 */
final class Version20210217122000 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("
             INSERT INTO section (`id`, `article_id`, `title`, `sub_title`, `position`, `status`, `created_date`, `updated_date`) VALUES (41, 15, 'MOV\\'ICI SE DÉVELOPPE SUR LES TERRITOIRES EN PARTENARIAT AVEC :\r\n', NULL, '4', '1', '2021-02-17 14:00:00', NULL);");
        $this->addSql("
             INSERT INTO paragraph (`id`, `section_id`, `text`, `position`, `status`, `created_date`,`updated_date`) VALUES 
             (54, 41,'</br></br><img style=\"width:250px;\" src= \\'/images/pages/articles/Logo_3CM_CC.jpg \\'/><img style=\"width:150px;\" src= \\'/images/pages/articles/Logo-Grand-Chambery.jpg \\'/><img style=\"width:250px;\" src= \\'/images/pages/articles/VRD-logo-web.jpg \\'/>', '1', '1', '2021-02-17 14:00:00', NULL);");
        $this->addSql("
             UPDATE `paragraph` SET `text` = '<br/><p>Au delà des longs trajets, il n\\'est plus pensable aujourd\\'hui d\\'utiliser un véhicule polluant tous les jours sans le mutualiser et partager ainsi ses trajets courtes distances.</p>' WHERE `paragraph`.`id` = 41;");
        $this->addSql("
             UPDATE `paragraph` SET `text` = '<br/><h4>Local</h4><p>Un nouveau service de transport de proximité, gratuit, au cœur de chacun de nos territoires.</p>' WHERE `paragraph`.`id` = 43;");
        $this->addSql("
             UPDATE `paragraph` SET `text` = '<br/><h4>Service public</h4><p>Notre intérêt est environnemental et social, uniquement.</p>' WHERE `paragraph`.`id` = 44;");
        $this->addSql("
             UPDATE `paragraph` SET `text` = '<br/><h4>Ensemble, on va plus loin</h4><p>Partager nos trajets quotidiens, c\\'est bien plus qu\\'un service mutuel : ce sont des échanges, des rencontres et des économies !</p>' WHERE `paragraph`.`id` = 45;");
        $this->addSql("
             UPDATE `paragraph` SET `text` = '<br/><h4>Innovation</h4><p>Nos fonctionnalités rendent vos covoiturages plus simples et plus sûrs : temps réel, paiement en ligne, certification.</p>' WHERE `paragraph`.`id` = 46;");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
