UPDATE `article` SET `title` = 'MOV\'ICI' WHERE `article`.`id` = 15;

INSERT INTO `section` (`id`, `article_id`, `title`, `sub_title`, `position`, `status`, `created_date`, `updated_date`) VALUES
(34, 15, 'SE DÉPLACER LOCAL GRÂCE AU COVOITURAGE RÉGIONAL', NULL, 2, 1, '2020-06-29 09:41:58', NULL),
(35, 15, 'PARTAGEONS NOS TRAJETS ET NOS VALEURS', NULL, 3, 1, '2020-06-29 09:43:59', NULL),
(36, 15, NULL, NULL, 1, 1, NULL, NULL);

INSERT INTO `paragraph` (`id`, `section_id`, `text`, `position`, `status`, `created_date`, `updated_date`) VALUES
(41, 34, '<p>Au delà des longs trajets, il n\'est plus pensable aujourd\'hui d\'utiliser un véhicule polluant tous les jours sans le mutualiser et partager ainsi ses trajets courtes distances.</p>', 1, 0, '2020-06-29 09:42:29', NULL),
(42, 34, '<p>Avec MOV’ICI, la Région Auvergne-Rhône-Alpes s’engage en faveur du covoiturage et complète son offre de mobilité au cœur des territoires. Ce service public de proximité, gratuit, permet de se déplacer partout et quand on veut : pour aller au travail, pour rendre visite à des amis, pour aller au festival d’à côté : <u>covoiturons local</u> !</p>', 1, 0, '2020-06-29 09:43:31', NULL),
(43, 35, '<p><h4>Local</h4></p><p>Un nouveau service de transport de proximité, gratuit, au cœur de chacun de nos territoires.</p>', 1, 0, '2020-06-29 09:44:52', NULL),
(44, 35, '<p><h4>Service public</h4></p><p>Notre intérêt est environnemental et social, uniquement.</p>', 2, 0, '2020-06-29 09:45:26', '2020-06-29 09:45:53'),
(45, 35, '<p><h4>Ensemble, on va plus loin</h4></p><p>Partager nos trajets quotidiens, c\'est bien plus qu\'un service mutuel : ce sont des échanges, des rencontres et des économies !</p>', 3, 0, '2020-06-29 09:46:27', NULL),
(46, 35, '<p><h4>Innovation</h4></p><p>Nos fonctionnalités rendent vos covoiturages plus simples et plus sûrs : temps réel, paiement en ligne, certification.</p>', 4, 0, '2020-06-29 09:47:06', NULL),
(47, 36, '<p style=\'text-align:center;\'><img src=\'/images/pages/home/logo-region-auvergne-rhone-alpes.png\' alt=\'\' /></p>', 1, 1, '2020-06-29 00:00:00', NULL);