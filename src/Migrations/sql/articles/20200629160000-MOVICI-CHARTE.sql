DELETE FROM `paragraph` WHERE `paragraph`.`section_id` = 13;
DELETE FROM `section` WHERE `section`.`id` = 13;

UPDATE `paragraph` SET `text` = '<p class=\"caption\">Retrouvez toutes les informations relatives aux données personnelles dans la <a href=\'/docs/200629_Notice d_information_données_Région.pdf\' title=\"Notice d\'information\" target="blank_">notice d\'information</a>.</p>' WHERE `paragraph`.`id` = 11;