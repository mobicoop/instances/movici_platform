-- Ticket#21357
-- update article linkedin
UPDATE `article` SET `title` = "Pollution de l'air", `i_frame` = "<iframe src=\"https://www.linkedin.com/embed/feed/update/urn:li:share:6626407731706245120\" height=\"360\" width=\"504\" frameborder=\"0\" allowfullscreen=\"\" title=\"Post intégré\"></iframe>" WHERE `article`.`id` = 1000;
UPDATE `article` SET `title` = "Covoiturage", `i_frame` = "<iframe src=\"https://www.linkedin.com/embed/feed/update/urn:li:share:6579300237888765952\" height=\"265\" width=\"504\" frameborder=\"0\" allowfullscreen=\"\" title=\"Post intégré\"></iframe>" WHERE `article`.`id` = 1001;
UPDATE `article` SET `title` = "Le Léman Express arrive le 15 décembre", `i_frame` = "<iframe src=\"https://www.linkedin.com/embed/feed/update/urn:li:share:6610933246232403969\" height=\"398\" width=\"504\" frameborder=\"0\" allowfullscreen=\"\" title=\"Post intégré\"></iframe>" WHERE `article`.`id` = 1002;
