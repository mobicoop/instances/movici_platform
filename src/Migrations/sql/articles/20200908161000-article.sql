UPDATE `article` SET `title`="Covoiturage Auvergne" WHERE `id`=9;
INSERT INTO `section` (`id`, `article_id`, `title`, `sub_title`, `position`, `status`, `created_date`, `updated_date`) VALUES
(40, 9, NULL, NULL, 1, 1, '2020-09-08 16:16:40', NULL);
INSERT INTO `paragraph` (`id`, `section_id`, `text`, `position`, `status`, `created_date`, `updated_date`) VALUES
(53, 40, '<p>Covoiturage Auvergne<br />Adresse :<br />33 rue de Vertaizon<br />63000 Clermont-Ferrand</p><p>Contacts:<br />04.73.90.47.93<br /><a class="email" href="mailto:contact@covoiturageauvergne.net">contact@covoiturageauvergne.net</a></p>', 1, 1, '2020-09-08 16:17:19', NULL);
