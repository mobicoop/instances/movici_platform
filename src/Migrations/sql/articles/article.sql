UPDATE `article` SET `title` = 'Charte et conditions générales d\'utilisation' WHERE `article`.`id` = 1;
UPDATE `article` SET `title` = 'Mentions légales' WHERE `article`.`id` = 4;
UPDATE `article` SET `title` = 'Questions fréquentes' WHERE `article`.`id` = 10;
UPDATE `article` SET `title` = 'Boîte à outils' WHERE `article`.`id` = 11;
UPDATE `article` SET `title` = 'Communautés' WHERE `article`.`id` = 12;

INSERT INTO `article` (`id`, `title`, `status`, `created_date`, `updated_date`, `i_frame`) VALUES
(1000, 'Tomorrow Jobs', 1, '2020-02-14 11:40:00', NULL, '<iframe src=\"https://www.linkedin.com/embed/feed/update/urn:li:share:6634013653656051712\" height=\"600\" width=\"504\" frameborder=\"0\" allowfullscreen=\"\" title=\"Post intégré\"></iframe>'),
(1001, 'Le 1er tour des élections municipales de 2020', 1, '2020-02-14 11:42:00', NULL, '<iframe src=\"https://www.linkedin.com/embed/feed/update/urn:li:share:6623207035737526272\" height=\"600\" width=\"504\" frameborder=\"0\" allowfullscreen=\"\" title=\"Post intégré\"></iframe>'),
(1002, 'Netflix opening in Paris', 1, '2020-02-14 11:43:00', NULL, '<iframe src=\"https://www.linkedin.com/embed/feed/update/urn:li:share:6624023795525124096\" height=\"370\" width=\"504\" frameborder=\"0\" allowfullscreen=\"\" title=\"Post intégré\"></iframe>');
