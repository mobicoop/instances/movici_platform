<?php declare(strict_types=1);

// namespace DoctrineMigrations; For dev
namespace App\Migrations; // For test/prod

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ticket #22460
 */
final class Version20210111153200 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('UPDATE paragraph SET text=\'<ol><li>Télécharger le formulaire <a href="/images/communication/2020-Formulaire création communauté.pdf" title="Notice dʼinformation" target="blank_">Création de communauté</a>,</li><li>Retourner votre projet de création via le formulaire par mail à <a href="mailto:covoiturage@auvergnerhonealpes.fr" rel="noopener noreferrer" target="_blank">covoiturage@auvergnerhonealpes.fr</a>, </li><li>Nous étudierons votre candidature sous un délai de 3 semaines après réception de votre demande.</li></ol><p><br></p><p>Une communauté est créée pour une période de 12 mois (renouvelable). </p><p>Pour la création de communauté,il est impératif de :</p><ul><li>Nommer un référent pour administrer et gérer la communauté,</li><li>Effectuer des animations régulières (en fournissant un calendrier dʼactions),</li><li>Promouvoir régulièrement la pratique du covoiturage.</li></ul>\' WHERE paragraph.id=\'35\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
