<?php

declare(strict_types=1);

// namespace DoctrineMigrations; For dev

namespace App\Migrations; // For test/prod

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ticket #23129.
 */
final class Version20220517152311 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 1;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 2;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 3;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 5;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 8;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 11;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 13;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 16;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 19;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 22;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 24;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 35;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 39;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 43;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 47;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 52;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 56;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 60;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 63;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 64;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 83;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 84;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 86;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 90;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 91;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 94;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 98;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 101;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 104;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 107;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 110;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 113;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 116;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 119;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 122;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 128;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 129;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 130;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 131;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 132;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 133;');
        $this->addSql('UPDATE `notification` SET `alt` = 1 WHERE `notification`.`id` = 137;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
    }
}
